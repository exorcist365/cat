import Control.Monad ((>=>))
import System.Environment (getArgs)

main :: IO ()
main = getArgs >>= (mapM readFile >=> (putStr . unwords))
